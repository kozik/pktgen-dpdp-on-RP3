Fix compilation error of pktgen-dpdk 3.5.1.

More info in [Magazyn Programista 5/2018 (72)](https://programistamag.pl/malinowa-siec-czyli-dpdk-na-raspberry-pi-3/).